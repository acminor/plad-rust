PROJECT_ROOT=$PWD

function install_linuxbrew {
    git clone https://github.com/Homebrew/brew ${PROJECT_ROOT}/_brew
    mkdir ${PROJECT_ROOT}/_brew/bin
    ln -s ${PROJECT_ROOT}/_brew/Homebrew/bin/brew ${PROJECT_ROOT}/_brew/bin
    eval $(${PROJECT_ROOT}/_brew/bin/brew shellenv)
}

function install_deps {
    brew install --build-from-source ./bubblewrap.rb
}

function construct_sandbox {
    # necessary since working with GNU/Linux operating systems in user mode which lead
    # to some write permission issues (even as owner of the files)
    chmod -R +w _downloads
    chmod -R +w files

    rm -rf _downloads
    rm -rf files

    mkdir _downloads
    mkdir files

    # https://stackoverflow.com/a/16261265
    # on why we use -Lk
    curl -Lk https://github.com/CentOS/sig-cloud-instance-images/archive/CentOS-8.1.1911-x86_64.zip \
         --output _downloads/centos8.zip

    cd _downloads
    unzip centos8.zip

    cd sig-cloud-instance-images-CentOS-8.1.1911-x86_64/docker

    cp CentOS-8-Container-8.1.1911-20200113.3-layer.x86_64.tar.xz ../../../files/layer.tar.xz
    cd ../../../

    cd files
    tar -xf layer.tar.xz
    rm -f layer.tar.xz

    cd ..
    chmod -R +w files
}

function setup_sandbox {
    cp install_pkgs.sh files/
    ./run.sh root-bash-run './install_pkgs.sh'
    ./run.sh root-bash-run 'mkdir /var/apps; cd /var/apps; git clone https://gitlab.com/acminor/plad-rust'
}

install_linuxbrew
install_deps
construct_sandbox
setup_sandbox
