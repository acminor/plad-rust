# Documentation: https://docs.brew.sh/Formula-Cookbook
#                https://rubydoc.brew.sh/Formula
class Bubblewrap < Formula
  desc "Bubblewrap is a userspace sandboxing tool."
  homepage "https://github.com/containers/bubblewrap"
  url "https://github.com/containers/bubblewrap/archive/v0.4.1.zip"
  version "0.4.1"
  sha256 "64587f51369254964b81d6de528a2314fa40e30e63f017da4f34c12b37f75ea8"
  license "LGPL2"
  
  depends_on "libcap"

  def install
    # ENV.deparallelize  # if your formula fails when building in parallel
    # Remove unrecognized options if warned by configure
    system "./autogen.sh", "--disable-debug",
                           "--disable-dependency-tracking",
                           "--disable-silent-rules",
                           "--disable-man",
                           "--without-bash-completion-dir",
                           "--prefix=#{prefix}"
    system "make"
    system "make", "install"
  end

  test do
    # `test do` will create, run in and delete a temporary directory.
    #
    # This test will fail and we won't accept that! For Homebrew/homebrew-core
    # this will need to be a test that verifies the functionality of the
    # software. Run the test with `brew test bubblewrap`. Options passed
    # to `brew install` such as `--HEAD` also need to be provided to `brew test`.
    #
    # The installed folder is not in the path, so use the entire path to any
    # executables being tested: `system "#{bin}/program", "do", "something"`.
    system "false"
  end
end
