#!/bin/bash

shopt -s expand_aliases

alias bw-wrap="bwrap \
	--bind ${PWD}/files / \
	--bind / /ext-computer \
	--tmpfs /tmp \
  --ro-bind /sys /sys \
	--ro-bind /proc /proc \
  --ro-bind /etc/resolv.conf{,} \
	--dev /dev \
  --ro-bind /usr/lib64/dri /usr/lib64/dri \
  --bind /dev/dri /dev/dri \
  --dev-bind /dev/nvidia-uvm /dev/nvidia-uvm \
  --dev-bind /dev/nvidia-modeset{,} \
  --dev-bind /dev/nvidia0{,} \
  --dev-bind /dev/nvidiactl{,} \
  --ro-bind /usr/lib64/libcuda.so.1 /usr/lib64/libcuda.so.1 \
  --ro-bind /usr/lib64/libnvidia-fatbinaryloader.so.440.33.01{,} \
  --ro-bind /usr/lib64/libnvidia-ptxjitcompiler.so.1{,} \
  --ro-bind /usr/local/cuda /opt/cuda-samples \
  --bind /tmp/.X11-unix/X0 /tmp/.X11-unix/X0 \
  --setenv DISPLAY :0 \
  --setenv AF_TRACE unified \
	--setenv LD_LIBRARY_PATH \$LD_LIBRARY_PATH:/opt/arrayfire/lib64:/usr/local/cuda/lib64 \
  --setenv HOME /root \
  --setenv PATH /bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin:/usr/local/cuda/bin \
  --setenv USER root \
  --setenv LD_LIBRARY_PATH /lib:/lib64:/usr/lib:/usr/lib64:/usr/local/lib:/usr/local/lib64:/opt/arrayfire/lib64:/usr/local/cuda/lib64:/usr/lib64/xorg/modules/extensions/:/usr/lib64/xorg/modules/drivers"

case $1 in
    bash)
        bw-wrap --chdir / bash
    ;;
    root-bash)
        bw-wrap --chdir / --uid 0 --gid 0 bash
    ;;
    root-bash-run)
        shift

        bw-wrap --chdir / --uid 0 --gid 0 -- bash -c "$@"
        ;;
    edit)
        shift
        bw-wrap --chdir /var/apps/plad-rust -- bash --init-file "/root/.bash_profile"
    ;;
esac
