#!/bin/bash

function install_arrayfire {
    af_download_link="https://arrayfire.s3.amazonaws.com/3.7.1/ArrayFire-v3.7.1-1_Linux_x86_64.sh"
    af_file="ArrayFire-v3.7.1-1_Linux_x86_64.sh"

    # TODO replace by generic wget
    if [[ ! -e ${af_file} ]]
    then
        curl ${af_download_link} --output ${af_file}
    fi

    sh ${af_file} --include-subdir --prefix=/opt
}

function install_rust {
    # fix any incorrect permissions
    # (occurring for some reason, unsure why
    #  - maybe docker image setup or bind mounting changing permissions)
    chmod -R +w /root 2> /dev/null

    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- --default-toolchain nightly -y

    source ~/.bash_profile
    source ~/.bashrc
    source ~/.profile
}

function install_packages {
    centos_pkgs=$(cat <<EOF
        git capnproto
        python3-devel
        python3-tkinter
        sqlite gperftools-devel
EOF
)

    # fix any incorrect permissions
    # (occurring for some reason, unsure why
    #  - maybe docker image setup or bind mounting changing permissions)
    chmod -R +w /usr 2> /dev/null
    chmod -R +w /opt 2> /dev/null
    chmod -R +w /etc 2> /dev/null
    chmod -R +w /lib 2> /dev/null
    chmod -R +w /lib64 2> /dev/null
    chmod -R +w /bin 2> /dev/null
    chmod -R +w /sbin 2> /dev/null

    dnf update -y
    dnf install -y https://dl.fedoraproject.org/pub/epel/epel-release-latest-8.noarch.rpm
    dnf groupinstall -y "Development Tools"
    dnf install -y ${centos_pkgs}

    pip3 install matplotlib
}

function main {
    install_packages
    echo
    install_arrayfire
    echo
    install_rust
    echo
}

main $@
